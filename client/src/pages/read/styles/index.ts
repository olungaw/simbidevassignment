import { Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: `grid`,
    gridTemplateRows: `repeat(2, auto)`,
    gridRowGap: theme.spacing(2),
    maxWidth: theme.breakpoints.values.sm,
    padding: theme.spacing(4),
    position: `relative`,
    top: 0,
  },
}));
