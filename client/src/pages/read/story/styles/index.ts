import { Theme, makeStyles } from '@material-ui/core/styles';

import green from '@material-ui/core/colors/green';

export const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: `flex`,
    flexWrap: `wrap`,
    [`& > *`]: {
      marginInlineEnd: `${theme.spacing(0.5)}px`,
    },
  },
  loading: {
    display: `flex`,
    flexDirection: `column`,
    alignItems: `center`,
  },
  highlight: {
    backgroundColor: green[400],
  },
  pageChange: {
    display: `flex`,
    justifyContent: `space-between`,
  },
}));
