import { ReduxDispatch, ReduxState } from '../../../redux/store';
import {
  WordWithState,
  transcriptionSlice,
} from '../../../redux/reducers/transcription';
import { useDispatch, useSelector } from 'react-redux';

import Button from '@material-ui/core/Button';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CircularProgress from '@material-ui/core/CircularProgress';
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useCheckWords } from './hooks';
import { useStyles } from './styles';

interface IProps {
  words: Array<Array<WordWithState>>;
}

export const Story: React.FC<IProps> = ({ words }) => {
  const classes = useStyles();
  const pageNumber = useSelector((state: ReduxState): number => {
    return state.transcription.currentPageNumber;
  });

  useCheckWords();

  const dispatch = useDispatch<ReduxDispatch>();

  const goBack = (): void => {
    dispatch(transcriptionSlice.actions.decrementPageNumber());
  };

  const goNext = (): void => {
    dispatch(transcriptionSlice.actions.incrementPageNumber());
  };

  return words.length > 0 ? (
    <>
      <Typography className={classes.container} component="p">
        {words[pageNumber].map(
          ({ word, matched }: WordWithState, index: number): JSX.Element => (
            <span
              className={matched ? classes.highlight : ``}
              key={`${word}-${index}`}
            >
              {word}
            </span>
          )
        )}
      </Typography>
      <div className={classes.pageChange}>
        <Button
          disabled={pageNumber <= 0}
          onClick={goBack}
          startIcon={<ChevronLeftIcon />}
        >
          Back
        </Button>
        <Button
          disabled={pageNumber + 1 >= words.length}
          onClick={goNext}
          endIcon={<ChevronRightIcon />}
        >
          Next
        </Button>
      </div>
    </>
  ) : (
    <div className={classes.loading}>
      <CircularProgress color="primary" />
      <Typography>Getting everything ready...</Typography>
    </div>
  );
};
