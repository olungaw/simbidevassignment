import { ReduxDispatch, ReduxState } from '../../../../redux/store';
import {
  Transcription,
  WordWithState,
  transcriptionSlice,
} from '../../../../redux/reducers/transcription';
import { useDispatch, useSelector } from 'react-redux';

import React from 'react';
import { updateWordsState } from './utils';

interface IUseCheckWordsState {
  words: Array<Array<WordWithState>>;
  currentWordIndex: number;
  currentPageNumber: number;
  transcribed?: Transcription;
}

export const useCheckWords = (): void => {
  const {
    words,
    currentWordIndex,
    currentPageNumber,
    transcribed,
  } = useSelector(
    ({
      transcription: {
        words,
        currentWordIndex,
        currentPageNumber,
        transcribed,
      },
    }: ReduxState): IUseCheckWordsState => ({
      words,
      transcribed,
      currentWordIndex,
      currentPageNumber,
    })
  );

  const dispatch = useDispatch<ReduxDispatch>();

  React.useEffect((): void => {
    if (transcribed) {
      const { updatedWords, nextStartCheckIndex } = updateWordsState({
        transcribed,
        wordsToCheck: words[currentPageNumber],
        startCheckIndex: currentWordIndex,
      });

      dispatch(
        transcriptionSlice.actions.updateWords({
          updatedWords: [
            ...words.slice(0, currentPageNumber),
            updatedWords,
            ...words.slice(currentPageNumber + 1),
          ],
          nextStartCheckIndex,
        })
      );
    }
  }, [transcribed, currentWordIndex, currentPageNumber, words, dispatch]);
};
