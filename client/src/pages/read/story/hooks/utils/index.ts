import {
  Transcription,
  WordWithState,
} from '../../../../../redux/reducers/transcription';

const charactersRegex = new RegExp(/[^a-z]/gi);

const keepOnlyLowercaseCharacters = (word: string): string => {
  return word.replace(charactersRegex, ``);
};

const checkWords = (
  wordsToCheck: Array<WordWithState>,
  transcriptionWords: Array<string>
): Array<WordWithState> => {
  const words = wordsToCheck.map(
    (item: WordWithState): WordWithState => ({
      word: item.word,
      matched: item.matched,
    })
  );

  transcriptionWords.forEach((word: string, index: number): void => {
    if (
      words[index] &&
      keepOnlyLowercaseCharacters(words[index].word) === word
    ) {
      words[index].matched = true;
    }
  });

  return words;
};

interface IUpdateWordsArgs {
  transcribed: Transcription;
  wordsToCheck: Array<WordWithState>;
  startCheckIndex: number;
}

interface IUpdateWordsResult {
  updatedWords: Array<WordWithState>;
  nextStartCheckIndex: number;
}

export const updateWordsState = ({
  transcribed,
  wordsToCheck,
  startCheckIndex,
}: IUpdateWordsArgs): IUpdateWordsResult => {
  const transcriptionWords = transcribed.transcription.split(` `);
  const requiresCheck = wordsToCheck.slice(startCheckIndex);

  const words = checkWords(
    requiresCheck,
    transcriptionWords.map(keepOnlyLowercaseCharacters)
  );

  const updatedWords = [...wordsToCheck.slice(0, startCheckIndex), ...words];

  return {
    updatedWords,
    nextStartCheckIndex: transcribed.final
      ? startCheckIndex + transcriptionWords.length
      : startCheckIndex,
  };
};
