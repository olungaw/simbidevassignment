import { Error } from './error';
import { IUseRecord } from './hooks';
import React from 'react';
import { RecordButton } from './button';

export const Record: React.FC<IUseRecord> = ({
  error,
  clearError,
  recording,
  toggleRecording,
}) => {
  return (
    <>
      <Error message={error} clearError={clearError} />
      <RecordButton recording={recording} toggleRecording={toggleRecording} />
    </>
  );
};
