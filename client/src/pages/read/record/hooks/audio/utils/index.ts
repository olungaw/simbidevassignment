const getUserMedia = (
  constraints: MediaStreamConstraints
): Promise<MediaStream> => {
  // * Handles older browsers with no Web Audio API support
  if (
    navigator.mediaDevices === undefined ||
    navigator.mediaDevices.getUserMedia === undefined
  ) {
    throw new Error(
      `This browser does not support recording from your microphone. Try using a different browser`
    );
  }

  return navigator.mediaDevices.getUserMedia(constraints);
};

export const getMediaStreamAsync = async (): Promise<MediaStream> => {
  let mediaStream: void | undefined | MediaStream;

  mediaStream = await getUserMedia({ audio: true });

  if (!mediaStream) {
    throw new Error(
      `Could not record audio. Try again with a different browser`
    );
  }

  return mediaStream;
};

export const createBuffer = (buffer: Float32Array): ArrayBufferLike => {
  const SAMPLE_RATE = 44100;
  const OUT_SAMPLE_RATE = 16000;

  const sampleRateRatio = SAMPLE_RATE / OUT_SAMPLE_RATE;
  const newLength = Math.round(buffer.length / sampleRateRatio);
  const result = new Int16Array(newLength);

  let offsetResult = 0;
  let offsetBuffer = 0;
  while (offsetResult < result.length) {
    const nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);

    let total = 0;
    let count = 0;
    for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
      total += buffer[i];
      count++;
    }

    result[offsetResult] = Math.min(1, total / count) * 0x7fff;
    offsetResult++;
    offsetBuffer = nextOffsetBuffer;
  }

  return result.buffer;
};
