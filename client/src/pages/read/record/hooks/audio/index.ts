import { createBuffer, getMediaStreamAsync } from './utils';

import { EventEnum } from '../../../../../utils';
import React from 'react';
import { ReduxDispatch } from '../../../../../redux/store';
import { audioSlice } from '../../../../../redux/reducers/audio';
import { useDispatch } from 'react-redux';

interface IArgs {
  recording: boolean;
  websocket: SocketIOClient.Socket;
  error?: string;
}

export const useAudio = ({ recording, error, websocket }: IArgs): void => {
  const audioContext = React.useRef<AudioContext>(new AudioContext());
  const bufferSize = React.useRef<number>(2048);

  const dispatch = useDispatch<ReduxDispatch>();

  React.useEffect((): (() => void) => {
    const context = audioContext.current;

    return (): void => {
      context.close();
    };
  }, []);

  React.useEffect((): (() => void) => {
    let stream: undefined | MediaStream;
    let source: undefined | MediaStreamAudioSourceNode;
    let processor: undefined | ScriptProcessorNode;
    const context = audioContext.current;

    if (recording && error === undefined) {
      const handleError = (err: Error): void => {
        console.error(err);
        dispatch(audioSlice.actions.setError(err));
      };

      getMediaStreamAsync()
        .then((mediaStream: MediaStream): void => {
          stream = mediaStream;

          //* Instantiate source
          source = context.createMediaStreamSource(mediaStream);

          //* Instantiate processor
          processor = context.createScriptProcessor(bufferSize.current, 1, 1);

          //* Let the source know its processor
          source.connect(processor);

          //* Connect processor to audio context
          processor.connect(context.destination);

          // * Start recording
          context.resume();

          //* Prepare to receive audio data
          processor.onaudioprocess = (event: AudioProcessingEvent): void => {
            if (recording) {
              websocket.emit(
                EventEnum.AUDIO_DATA_EVENT,
                createBuffer(event.inputBuffer.getChannelData(0))
              );
            }
          };
        })
        .catch(handleError);
    }

    return (): void => {
      // * Stop media stream
      stream?.getTracks().forEach((track: MediaStreamTrack): void => {
        track.stop();
      });

      // * Disconnect source from processor
      if (processor) {
        source?.disconnect(processor);
      }

      // * Disconnect processor from audio context
      processor?.disconnect(context.destination);
    };
  }, [dispatch, recording, error, websocket]);
};
