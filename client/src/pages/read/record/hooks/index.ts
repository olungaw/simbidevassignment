import { ReduxDispatch, ReduxState } from '../../../../redux/store';
import { useDispatch, useSelector } from 'react-redux';

import React from 'react';
import { audioSlice } from '../../../../redux/reducers/audio';
import { getMediaStreamAsync } from './audio/utils';
import { transcriptionSlice } from '../../../../redux/reducers/transcription';
import { useAudio } from './audio';
import { useWebSocket } from './webSocket';

export interface IUseRecord {
  error?: string;
  recording: boolean;
  toggleRecording: () => void;
  clearError: () => void;
}

export const useRecord = (): IUseRecord => {
  const [mediaStream, setMediaStream] = React.useState<MediaStream>();

  const error = useSelector((state: ReduxState): undefined | string => {
    return state.audio.error?.message ?? state.transcription.error?.message;
  });

  const recording = useSelector((state: ReduxState): boolean => {
    return state.audio.recording;
  });

  const dispatch = useDispatch<ReduxDispatch>();

  const websocket = useWebSocket({ recording, error });
  useAudio({ recording, error, websocket });

  React.useEffect((): void => {
    if (recording) {
      const handleError = (error: Error): void => {
        console.error(error);
        dispatch(audioSlice.actions.setError(error));
      };

      getMediaStreamAsync()
        .then((stream: MediaStream): void => {
          setMediaStream(stream);
        })
        .catch(handleError);
    }
  }, [dispatch, recording]);

  React.useEffect((): void => {
    if (!recording) {
      mediaStream?.getTracks().forEach((track: MediaStreamTrack): void => {
        track.stop();
      });
      setMediaStream(undefined);
    }
  }, [recording, mediaStream]);

  const toggleRecording = (): void => {
    dispatch(audioSlice.actions.toggleRecording());
  };

  const clearError = (): void => {
    dispatch(audioSlice.actions.clearError());
    dispatch(transcriptionSlice.actions.clearError());
  };

  return {
    error,
    recording,
    toggleRecording,
    clearError,
  };
};
