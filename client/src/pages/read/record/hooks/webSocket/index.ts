import { EventEnum, SERVER_URL } from '../../../../../utils';
import {
  GoogleSpeechAPIResponse,
  Transcription,
  transcriptionSlice,
} from '../../../../../redux/reducers/transcription';

import React from 'react';
import { ReduxDispatch } from '../../../../../redux/store';
import SocketIOClient from 'socket.io-client';
import { audioSlice } from '../../../../../redux/reducers/audio';
import { useDispatch } from 'react-redux';

interface IArgs {
  recording: boolean;
  error?: string;
}

export const useWebSocket = ({
  recording,
  error,
}: IArgs): SocketIOClient.Socket => {
  const client = React.useRef(
    SocketIOClient(SERVER_URL, { autoConnect: true })
  );

  const dispatch = useDispatch<ReduxDispatch>();

  const onGoogleCloudStreamTranscribed = React.useCallback(
    (data: Transcription): void => {
      dispatch(transcriptionSlice.actions.addTranscription(data));
    },
    [dispatch]
  );

  const onGoogleCloudStreamError = React.useCallback(
    (response: GoogleSpeechAPIResponse): void => {
      dispatch(transcriptionSlice.actions.setError(response));
      dispatch(audioSlice.actions.stopRecording());
    },
    [dispatch]
  );

  const stopListening = React.useCallback(
    (webSocket: SocketIOClient.Socket): void => {
      webSocket.off(
        EventEnum.GOOGLE_CLOUD_STREAM_TRANSCRIBED_EVENT,
        onGoogleCloudStreamTranscribed
      );
      webSocket.off(
        EventEnum.GOOGLE_CLOUD_STREAM_ERROR_EVENT,
        onGoogleCloudStreamError
      );
    },
    [onGoogleCloudStreamTranscribed, onGoogleCloudStreamError]
  );

  React.useEffect((): (() => void) => {
    const webSocket = client.current;

    const onConnect = (): void => {
      console.log(`Server connected to client!`);
    };

    webSocket.on(`connect`, onConnect);

    const onDisconnect = (): void => {
      console.log(`Server disconnected!`);
    };

    webSocket.on(`disconnect`, onDisconnect);

    return (): void => {
      webSocket.off(`connect`, onConnect);
      webSocket.off(`disconnect`, onDisconnect);
      webSocket.close();
    };
  }, []);

  React.useEffect((): void => {
    if (recording && error === undefined) {
      client.current.emit(EventEnum.GOOGLE_CLOUD_STREAM_START_EVENT);
      client.current.on(
        EventEnum.GOOGLE_CLOUD_STREAM_TRANSCRIBED_EVENT,
        onGoogleCloudStreamTranscribed
      );
      client.current.on(
        EventEnum.GOOGLE_CLOUD_STREAM_ERROR_EVENT,
        onGoogleCloudStreamError
      );
    } else if (!recording || error) {
      client.current.emit(EventEnum.GOOGLE_CLOUD_STREAM_END_EVENT);
      stopListening(client.current);
    }
  }, [
    recording,
    error,
    stopListening,
    onGoogleCloudStreamTranscribed,
    onGoogleCloudStreamError,
  ]);

  return client.current;
};
