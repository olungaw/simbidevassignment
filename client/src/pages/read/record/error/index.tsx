import Alert from '@material-ui/lab/Alert';
import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';

interface IProps {
  message?: string;
  clearError: () => void;
}

export const Error: React.FC<IProps> = ({ message, clearError }) => {
  return (
    <Snackbar
      anchorOrigin={{ vertical: `top`, horizontal: `center` }}
      open={Boolean(message)}
      onClose={clearError}
    >
      <Alert severity="error" variant="filled" onClose={clearError}>
        {message}
      </Alert>
    </Snackbar>
  );
};
