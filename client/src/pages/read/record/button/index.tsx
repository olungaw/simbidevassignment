import Fab from '@material-ui/core/Fab';
import MicIcon from '@material-ui/icons/Mic';
import MicOffIcon from '@material-ui/icons/MicOff';
import React from 'react';
import { useStyles } from './styles';

interface IProps {
  recording: boolean;
  toggleRecording: () => void;
}

export const RecordButton: React.FC<IProps> = ({
  recording,
  toggleRecording,
}) => {
  const classes = useStyles();

  return (
    <Fab
      className={classes.button}
      color="primary"
      variant="extended"
      size="medium"
      onClick={toggleRecording}
    >
      {recording ? <MicOffIcon /> : <MicIcon />}
      {recording ? `Stop Recording` : `Start Recording`}
    </Fab>
  );
};
