import { Theme, makeStyles } from '@material-ui/core/styles';

import green from '@material-ui/core/colors/green';

export const useStyles = makeStyles((theme: Theme) => ({
  bold: {
    fontWeight: `bold`,
  },
  highlight: {
    backgroundColor: green[400],
  },
}));
