import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './styles';

export const Information: React.FC = () => {
  const classes = useStyles();

  return (
    <>
      <Typography variant="body2">
        Once you click <span className={classes.bold}>Start recording</span> The
        words you are able to pronounce correctly will be highlighted in{' '}
        <span className={classes.highlight}>green</span>.
      </Typography>
      <Typography variant="body2">
        Once you are done reading, click{' '}
        <span className={classes.bold}>Stop recording</span>.
      </Typography>
    </>
  );
};
