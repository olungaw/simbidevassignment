import {
  WordWithState,
  transcriptionSlice,
} from '../../../redux/reducers/transcription';

import React from 'react';
import { ReduxDispatch } from '../../../redux/store';
import { useDispatch } from 'react-redux';

interface IUseSetupStoryArgs {
  state?: any;
}

export const useSetupStory = ({ state }: IUseSetupStoryArgs): void => {
  const dispatch = useDispatch<ReduxDispatch>();

  React.useEffect((): void => {
    if (state) {
      const story = state as Record<string, string>;

      // * Remove key from location state
      const keyIndex = Object.keys(story).findIndex(
        (key: string): boolean => key === `key`
      );
      const storyWithoutKey = [
        ...Object.values(story).slice(0, keyIndex),
        ...Object.values(story).slice(keyIndex + 1),
      ];

      dispatch(
        transcriptionSlice.actions.setupStory({
          story: storyWithoutKey,
          words: storyWithoutKey.map(
            (page: string): Array<WordWithState> => {
              return page.split(` `).map(
                (word: string): WordWithState => ({
                  word,
                  matched: false,
                })
              );
            }
          ),
        })
      );
    }
  }, [state, dispatch]);
};
