import { RouteComponentProps, navigate } from '@reach/router';

import { Information } from './information';
import React from 'react';
import { Record } from './record';
import { ReduxState } from '../../redux/store';
import { Story } from './story';
import { WordWithState } from '../../redux/reducers/transcription';
import { useRecord } from './record/hooks';
import { useSelector } from 'react-redux';
import { useSetupStory } from './hooks';
import { useStyles } from './styles';

export const ReadPage: React.FC<RouteComponentProps> = ({ location }) => {
  React.useEffect((): void => {
    if (!location?.state) {
      navigate(`/`);
    }
  }, [location]);

  useSetupStory({
    state: location?.state,
  });

  const words = useSelector(
    (state: ReduxState): Array<Array<WordWithState>> =>
      state.transcription.words
  );

  const record = useRecord();
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Information />
      <Record {...record} />
      <Story words={words} />
    </div>
  );
};
