import { Book } from '../components/book';
import React from 'react';
import { RouteComponentProps } from '@reach/router';
import Typography from '@material-ui/core/Typography';
import { useStyles } from './styles';

export const HomePage: React.FC<RouteComponentProps> = () => {
  const classes = useStyles();

  return (
    <section className={classes.container}>
      <Typography variant="h6">Welcome to the Reading Platfom</Typography>
      <div className={classes.books}>
        <Book
          title="Software and Cake: A Terrible Combination!"
          blurb="Learn more about how learning to code can make you better at making cakes. Yes that's right, anyone who is good at code is good at making cakes. Let's show you how!"
          to="read/software-and-cake"
          content={[
            `First line goes here. Second line goes here. Third line goes here. Fourth lines goes here. Fifth line goes here...`,
            `First line goes here. Second line goes here. Third line goes here. Fourth lines goes here. Fifth line goes here...`,
            `First line goes here. Second line goes here. Third line goes here. Fourth lines goes here. Fifth line goes here...`,
          ]}
        />
        <Book
          title="The future of education"
          blurb="How school is making reading cool again. We are sick and tired of losing our education system. Let us teach you that school has so much more to offer!"
          to="read/future-of-education"
          content={[
            `Dear hiring manager:. Dogpile that we are running out of runway optimize the fireball goalposts execute . Guerrilla marketing get all your ducks in a row, yet low-hanging fruit, or crank this out globalize, and we need to get all stakeholders up to speed and in the right place. Drink from the firehose optics pull in ten extra bodies to help roll the tortoise, so bake it in or workflow ecosystem.`,
            `High performance keywords big picture or land the plane the right info at the right time to the right people root-and-branch review. Social currency low hanging fruit, for execute dunder mifflin yet highlights where do we stand on the latest client ask, for we need to socialize the comms with the wider stakeholder community. Where do we stand on the latest client ask hard stop, or moving the goalposts.`,
          ]}
        />
        <Book
          title="Have we forgotten? (Yes we have)"
          blurb="How memory can be improved from a young age. Memory is not something to be played around with but rather appreciated and better yet respected."
          to="read/have-we-forgotten"
          content={[
            `Drop-dead date. Player-coach high-level for please use "solutionise" instead of solution ideas! :) yet five-year strategic plan. Low engagement we don't want to boil the ocean but digitalize or please submit the sop and uat files by next monday and helicopter view, so a loss a day will keep you focus. Re-inventing the wheel moving the goalposts baseline low engagement and personal development.`,
            `Are we in agreeance sea change we need distributors to evangelize the new line to local markets, or lean into that problem please use "solutionise" instead of solution ideas!`,
            `Ramp up UX we need to dialog around your choice of work attire, yet goalposts personal development nor all hands on deck going forward. We need a recap by eod, cob or whatever comes first future-proof, beef up scope creep pivot out of the loop that's mint, well done.`,
            `Ramp up UX we need to dialog around your choice of work attire, yet goalposts personal development nor all hands on deck going forward. We need a recap by eod, cob or whatever comes first future-proof, beef up scope creep pivot out of the loop that's mint, well done.`,
            `Ramp up UX we need to dialog around your choice of work attire, yet goalposts personal development nor all hands on deck going forward. We need a recap by eod, cob or whatever comes first future-proof, beef up scope creep pivot out of the loop that's mint, well done.`,
            `Ramp up UX we need to dialog around your choice of work attire, yet goalposts personal development nor all hands on deck going forward. We need a recap by eod, cob or whatever comes first future-proof, beef up scope creep pivot out of the loop that's mint, well done.`,
          ]}
        />
        <Book
          title="Who am I?"
          blurb="A journey through the life of a stranger who met a stranger through another strangers. They later found out that they were all acquaintaces from long ago"
          to="read/who-am-i"
          content={[
            `High performance keywords big picture or land the plane the right info at the right time to the right people root-and-branch review. Social currency low hanging fruit, for execute dunder mifflin yet highlights where do we stand on the latest client ask, for we need to socialize the comms with the wider stakeholder community. Where do we stand on the latest client ask hard stop, or moving the goalposts.`,
            `High performance keywords big picture or land the plane the right info at the right time to the right people root-and-branch review. Social currency low hanging fruit, for execute dunder mifflin yet highlights where do we stand on the latest client ask, for we need to socialize the comms with the wider stakeholder community. Where do we stand on the latest client ask hard stop, or moving the goalposts.`,
            `High performance keywords big picture or land the plane the right info at the right time to the right people root-and-branch review. Social currency low hanging fruit, for execute dunder mifflin yet highlights where do we stand on the latest client ask, for we need to socialize the comms with the wider stakeholder community. Where do we stand on the latest client ask hard stop, or moving the goalposts.`,
            `High performance keywords big picture or land the plane the right info at the right time to the right people root-and-branch review. Social currency low hanging fruit, for execute dunder mifflin yet highlights where do we stand on the latest client ask, for we need to socialize the comms with the wider stakeholder community. Where do we stand on the latest client ask hard stop, or moving the goalposts.`,
            `Dear hiring manager:. Dogpile that we are running out of runway optimize the fireball goalposts execute . Guerrilla marketing get all your ducks in a row, yet low-hanging fruit, or crank this out globalize, and we need to get all stakeholders up to speed and in the right place. Drink from the firehose optics pull in ten extra bodies to help roll the tortoise, so bake it in or workflow ecosystem.`,
            `Personal development thinking outside the box a loss a day will keep you focus due diligence. Moving the goalposts dog and pony show take five, punch the tree, and come back in here with a clear head, nor we need to future-proof this win-win. Enough to wash your face no scraps hit the floor upstream selling, draw a line in the sand, so throughput, and old boys club. That ipo will be a game-changer hammer out performance review so nail it down for let's not solutionize this right now parking lot it quantity touch base. Low hanging fruit mobile friendly.`,
          ]}
        />
      </div>
    </section>
  );
};
