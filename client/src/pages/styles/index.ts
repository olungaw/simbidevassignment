import { Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: `flex`,
    flexDirection: `column`,
    alignItems: `center`,
    [`& > a`]: {
      marginBlockStart: `${theme.spacing()}px`,
    },
  },
  books: {
    maxWidth: `100%`,
    padding: theme.spacing(2),
    display: `grid`,
    gridTemplateRows: `repeat(3, 1fr)`,
    gridGap: theme.spacing(3),
    [`@media screen and (min-width: 380px)`]: {
      maxWidth: `80%`,
    },
    [`@media screen and (min-width: 500px)`]: {
      maxWidth: `60%`,
    },
    [`@media screen and (min-width: 700px)`]: {
      gridTemplateRows: `repeat(2, 1fr)`,
      gridTemplateColumns: `repeat(2, 1fr)`,
      maxWidth: `80%`,
    },
    [`@media screen and (min-width: 900px)`]: {
      maxWidth: `60%`,
    },
  },
}));
