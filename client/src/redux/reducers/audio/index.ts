import { CaseReducer, PayloadAction, createSlice } from '@reduxjs/toolkit';

type AudioSliceState = {
  recording: boolean;
  error?: Error;
};

const stopRecording: CaseReducer<AudioSliceState> = state => {
  return {
    ...state,
    recording: false,
  };
};

const toggleRecording: CaseReducer<AudioSliceState> = state => {
  return {
    ...state,
    recording: !state.recording,
  };
};

const setError: CaseReducer<AudioSliceState, PayloadAction<Error>> = (
  state,
  action
) => {
  return {
    ...state,
    error: action.payload,
  };
};

const clearError: CaseReducer<AudioSliceState> = state => {
  return {
    ...state,
    error: undefined,
  } as AudioSliceState;
};

export const audioSlice = createSlice({
  name: `audio`,
  initialState: {
    recording: false,
    data: undefined,
    error: undefined,
  } as AudioSliceState,
  reducers: {
    stopRecording,
    toggleRecording,
    setError,
    clearError,
  },
});
