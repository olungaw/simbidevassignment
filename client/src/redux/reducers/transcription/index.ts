import { CaseReducer, PayloadAction, createSlice } from '@reduxjs/toolkit';

export type Transcription = {
  transcription: string;
  final: boolean;
};

export type GoogleSpeechAPIResponse = {
  status: `success` | `error`;
  message: string;
};

export type WordWithState = {
  word: string;
  matched: boolean;
};

export type Page = {
  pageNumber: number;
  wordIndex: number;
};

type TranscriptionSliceState = {
  currentPageNumber: number;
  currentWordIndex: number;
  pages: Array<Page>;
  transcribed: Transcription | undefined;
  words: Array<Array<WordWithState>>;
  story: Array<string>;
  error?: GoogleSpeechAPIResponse;
};

const resetPageNumber: CaseReducer<TranscriptionSliceState> = state => {
  return {
    ...state,
    currentPageNumber: 0,
    currentWordIndex: state.pages[0].wordIndex,
  };
};

const incrementPageNumber: CaseReducer<TranscriptionSliceState> = state => {
  return {
    ...state,
    currentPageNumber: state.currentPageNumber + 1,
    currentWordIndex: state.pages[state.currentPageNumber + 1].wordIndex,
  };
};

const decrementPageNumber: CaseReducer<TranscriptionSliceState> = state => {
  return {
    ...state,
    currentPageNumber: state.currentPageNumber - 1,
    currentWordIndex: state.pages[state.currentPageNumber - 1].wordIndex,
  };
};

interface ISetupPayload {
  words: Array<Array<WordWithState>>;
  story?: Array<string>;
}

const setupStory: CaseReducer<
  TranscriptionSliceState,
  PayloadAction<ISetupPayload>
> = (state, action) => {
  return {
    ...state,
    story: action.payload.story ?? state.story,
    currentPageNumber: 0,
    currentWordIndex: 0,
    pages: (action.payload.story ?? state.story).map(
      (_: string): Page => ({
        pageNumber: 0,
        wordIndex: 0,
      })
    ),
    transcribed: undefined,
    words: action.payload.words,
  };
};

interface IUpdateWordsPayload {
  updatedWords: Array<Array<WordWithState>>;
  nextStartCheckIndex: number;
  story?: Array<string>;
}

const updateWords: CaseReducer<
  TranscriptionSliceState,
  PayloadAction<IUpdateWordsPayload>
> = (state, action) => {
  return {
    ...state,
    story: action.payload.story ?? state.story,
    currentWordIndex: action.payload.nextStartCheckIndex,
    pages: [
      ...state.pages.slice(0, state.currentPageNumber),
      {
        pageNumber: state.currentPageNumber,
        wordIndex: action.payload.nextStartCheckIndex,
      },
      ...state.pages.slice(state.currentPageNumber + 1),
    ],
    transcribed: undefined,
    words: action.payload.updatedWords,
  };
};

const addTranscription: CaseReducer<
  TranscriptionSliceState,
  PayloadAction<Transcription>
> = (state, action) => {
  return {
    ...state,
    error: undefined,
    transcribed: action.payload,
  };
};

const setError: CaseReducer<
  TranscriptionSliceState,
  PayloadAction<GoogleSpeechAPIResponse>
> = (state, action) => {
  return { ...state, error: action.payload };
};

const clearError: CaseReducer<TranscriptionSliceState> = state => {
  return { ...state, error: undefined };
};

export const transcriptionSlice = createSlice({
  name: `transcription`,
  initialState: {
    currentPageNumber: 0,
    currentWordIndex: 0,
    pages: [],
    transcribed: undefined,
    words: [],
    story: [],
    error: undefined,
  } as TranscriptionSliceState,
  reducers: {
    setupStory,
    updateWords,
    addTranscription,
    setError,
    clearError,
    resetPageNumber,
    incrementPageNumber,
    decrementPageNumber,
  },
});
