import { Action, ActionCreator } from 'redux';
import {
  ThunkAction,
  configureStore,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';

import { audioSlice } from '../reducers/audio';
import { transcriptionSlice } from '../reducers/transcription';

export const preloadedState = {};

export const reduxStore = configureStore({
  reducer: {
    audio: audioSlice.reducer,
    transcription: transcriptionSlice.reducer,
  },
  preloadedState,
  middleware: getDefaultMiddleware({
    immutableCheck: {
      warnAfter: 1000,
    },
    serializableCheck: false,
  }),
});

export type ReduxState = ReturnType<typeof reduxStore.getState>;

export type ReduxDispatch = typeof reduxStore.dispatch;

export type ReduxThunkActionCreator<
  LastDispatchedAction extends Action,
  LastActionPayload = any,
  NestedFunctionParameterType = any
> = ActionCreator<
  ThunkAction<
    Promise<LastDispatchedAction>,
    LastActionPayload,
    NestedFunctionParameterType,
    LastDispatchedAction
  >
>;
