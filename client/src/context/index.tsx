import { MaterialThemeContextProvider } from './materialTheme';
import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { reduxStore } from '../redux/store';

export const RootContextProvider: React.FC = ({ children }) => {
  return (
    <ReduxProvider store={reduxStore}>
      <MaterialThemeContextProvider>{children}</MaterialThemeContextProvider>
    </ReduxProvider>
  );
};
