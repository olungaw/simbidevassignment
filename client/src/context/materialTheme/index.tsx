import CssBaseline from '@material-ui/core/CssBaseline';
import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { createTheme } from '../../theme';

export const MaterialThemeContextProvider: React.FC = ({ children }) => {
  return (
    <ThemeProvider theme={createTheme()}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};
