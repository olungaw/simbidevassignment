import { HomePage } from '../pages';
import { Router as ReachRouter } from '@reach/router';
import React from 'react';
import { ReadPage } from '../pages/read';
import { RootContextProvider } from '../context';
import { useStyles } from './styles';

export const App: React.FC = () => {
  const classes = useStyles();

  return (
    <RootContextProvider>
      <main className={classes.main}>
        <ReachRouter>
          <HomePage path="/" />
          <ReadPage path="/read/:id" />
        </ReachRouter>
      </main>
    </RootContextProvider>
  );
};
