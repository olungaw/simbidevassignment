import { Theme, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => ({
  main: {
    display: `flex`,
    flexDirection: `column`,
    alignItems: `center`,
    minHeight: `100vh`,
    padding: theme.spacing(4, 0),
  },
}));
