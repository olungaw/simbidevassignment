import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  card: {
    display: `flex`,
    flexDirection: `column`,
    justifyContent: `space-between`,
  },
  action: {
    justifyContent: `flex-end`,
  },
});
