import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import React from 'react';
import { navigate } from '@reach/router';
import { useStyles } from './styles';

interface IProps {
  title: string;
  blurb: string;
  to: string;
  content: Array<string>;
}

export const Book: React.FC<IProps> = ({ title, blurb, to, content }) => {
  const classes = useStyles();

  const handleClick = (): void => {
    navigate(to, { state: content });
  };

  return (
    <Card className={classes.card}>
      <CardHeader title={title} />
      <CardContent>{blurb}</CardContent>
      <CardActions className={classes.action}>
        <Button color="primary" onClick={handleClick}>
          Start reading
        </Button>
      </CardActions>
    </Card>
  );
};
