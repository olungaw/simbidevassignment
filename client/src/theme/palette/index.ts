import { PaletteOptions } from '@material-ui/core/styles/createPalette';
import { PaletteType } from '@material-ui/core';

export const createPalette = (paletteType: PaletteType): PaletteOptions => ({
  type: paletteType,
  primary: {
    light: `#3949ab`,
    main: `#3949ab`,
    dark: `#3949ab`,
  },
  secondary: {
    light: `#00bfa5`,
    main: `#00bfa5`,
    dark: `#00bfa5`,
  },
});
