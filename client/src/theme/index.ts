import { Theme, createMuiTheme } from '@material-ui/core/styles';

import { createPalette } from './palette';
import { font } from './font';
import { overrides } from './overrides';

export const createTheme = (): Theme =>
  createMuiTheme({
    palette: createPalette(`light`),
    typography: {
      fontFamily: font.fontFamily,
    },
    overrides,
  });
