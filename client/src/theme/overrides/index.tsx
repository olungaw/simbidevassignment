import { Overrides } from '@material-ui/core/styles/overrides';

export const overrides: Overrides = {
  MuiButton: {
    root: {
      textTransform: `initial`,
    },
  },
  MuiFab: {
    root: {
      textTransform: `initial`,
    },
  },
};
