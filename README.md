# SimbiDev Assignment

Welcome to my SimbiDev assignment submission

Below are a few commands to get things going

## Environment variables

The server needs some environment variables from Google Cloud Platform to run. Make sure you create a project on Google Cloud Console, generate a service account and copy the relevant fields into your .env file. Follow the example given [here](https://cloud.google.com/speech-to-text/docs/quickstart-client-libraries). The fields required are described in the sample.env file in the server directory. The created .env file should be placed in the server directory.

## Development execution

If you would like to run the development build of the application run:

```bash
docker-compose up
```

After running the command open up this url in your browser: `http://localhost:3000`

## Production execution

If you would like to run the production build of the application run:

```bash
docker-compose -f docker-compose.prod.yml up
```

After running the command open up this url in your browser: `http://localhost:3000`
