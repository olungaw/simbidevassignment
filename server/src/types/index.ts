import { google } from '@google-cloud/speech/build/protos/protos';

export type GoogleSpeeechStreamingResult = google.cloud.speech.v1.IStreamingRecognitionResult;
