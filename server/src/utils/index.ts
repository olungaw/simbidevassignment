export enum EventEnum {
  AUDIO_DATA_EVENT = 'audio-data-event',
  GOOGLE_CLOUD_STREAM_START_EVENT = 'google-cloud-stream-start-event',
  GOOGLE_CLOUD_STREAM_END_EVENT = 'google-cloud-stream-end-event',
  GOOGLE_CLOUD_STREAM_TRANSCRIBED_EVENT = 'google-cloud-stream-transcribed-event',
  GOOGLE_CLOUD_STREAM_ERROR_EVENT = 'google-cloud-stream-error-event',
}
