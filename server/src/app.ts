import { EventEnum } from './utils';
import { GoogleSpeeechStreamingResult } from './types';
import Pumpify from 'pumpify';
import Socket from 'socket.io';
import dotenv from 'dotenv';
import express from 'express';
import http from 'http';
import speech from '@google-cloud/speech';

dotenv.config();

const speechClient = new speech.SpeechClient({
  projectId: process.env.GOOGLE_SPEECH_TO_TEXT_PROJECT_ID,
  credentials: {
    client_email: process.env.GOOGLE_SPEECH_TO_TEXT_CLIENT_EMAIL,
    private_key: process.env.GOOGLE_SPEECH_TO_TEXT_PRIVATE_KEY,
  },
});

const app = express();
const port = 4000;
const server = http.createServer(app);

const websocket = Socket(server);

websocket.on(`connection`, (client: SocketIO.Socket): void => {
  console.log(`Client Connected to server`);

  let recognizeStream: Pumpify | null = null;

  const startRecognitionStream = (client: SocketIO.Socket): void => {
    recognizeStream = speechClient
      .streamingRecognize({
        config: {
          encoding: `LINEAR16`,
          sampleRateHertz: 16000,
          languageCode: `en-US`,
          profanityFilter: true,
        },
        interimResults: true,
      })
      .on(`error`, (err: Error): void => {
        console.error(err);
        client.emit(EventEnum.GOOGLE_CLOUD_STREAM_ERROR_EVENT, {
          status: `error`,
          message: err.message,
        });
      })
      .on(`data`, (data: { results: Array<GoogleSpeeechStreamingResult> }) => {
        client.emit(EventEnum.GOOGLE_CLOUD_STREAM_TRANSCRIBED_EVENT, {
          transcription: data.results[0].alternatives?.[0].transcript ?? ``,
          final: data.results[0].isFinal ?? false,
        });

        if (data.results[0] && data.results[0].isFinal) {
          stopRecognitionStream();
          startRecognitionStream(client);
        }
      });
  };

  const stopRecognitionStream = (): void => {
    if (recognizeStream) {
      recognizeStream.end();
    }
    recognizeStream = null;
  };

  client.on(EventEnum.GOOGLE_CLOUD_STREAM_START_EVENT, (): void => {
    startRecognitionStream(client);
  });

  client.on(EventEnum.GOOGLE_CLOUD_STREAM_END_EVENT, (): void => {
    stopRecognitionStream();
  });

  client.on(EventEnum.AUDIO_DATA_EVENT, (data: ArrayBufferLike): void => {
    if (recognizeStream !== null) {
      recognizeStream.write(data);
    }
  });
});

server.listen(port, (): void => {
  console.log(`Server started on port: ${port}`);
});
